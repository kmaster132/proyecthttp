'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UsuarioSchema extends Schema {
  up () {
    this.create('usuarios', (table) => {
      table.increments()
      table.string('nombre').notNullable()
      table.string('edad').notNullable()
      table.timestamps('fecha')
    })
  }

  down () {
    this.drop('usuarios')
  }
}

module.exports = UsuarioSchema
