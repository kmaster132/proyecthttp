'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URLs and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.0/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')
const DB = use('Database')
Route.get('/', () => {
  return "hola";
})

Route.get('/ver', () => {
  return  DB.table('usuarios').select('*')
})


Route.post('/p','ControlLoginController.registro');

Route.post('/agregar','ControlLoginController.index');