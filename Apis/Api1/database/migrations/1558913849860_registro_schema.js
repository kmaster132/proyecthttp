'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class RegistroSchema extends Schema {
  up () {
    this.create('registros_s', (table) => {
      table.increments()
      table.string("nombre",255).notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('registros')
  }
}

module.exports = RegistroSchema
