'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class PuntosSchema extends Schema {
  up () {
    this.create('puntos', (table) => {
      table.increments()
      table.integer('usuarios_id').unsigned().references('id').inTable('usuarios')
      table.integer('Pt_ad').notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('puntos')
  }
}

module.exports = PuntosSchema
