//Modulos
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms'
import { GlobalCurrencyService } from './global.service';
import { HttpClientModule } from '@angular/common/http'

//Componentes
import { AppComponent } from './app.component';
import { CabeceraComponent } from './cabecera/cabecera.component';
import { AgregarComponent } from './CRUD/agregar/agregar.component';
import { VerComponent } from './CRUD/ver/ver.component';
import { AgregardirectComponent } from './CRUD/agregardirect/agregardirect.component';
import { VerdirectComponent } from './CRUD/verdirect/verdirect.component';
import { PerfilComponent } from './perfil/perfil.component';



 
@NgModule({
  declarations: [
    AppComponent,
    CabeceraComponent,
    AgregarComponent,
    VerComponent,
    AgregardirectComponent,
    VerdirectComponent,
    PerfilComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [GlobalCurrencyService],
  bootstrap: [AppComponent]
})
export class AppModule { }
